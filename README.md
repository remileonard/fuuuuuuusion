# README #

![Fusion](fusion.png)
## Fuuuuuuuusion ###

### Sync.sh

A quick bash script to merge multiple git repository in one keeping complete commit history

#### How do I get set up? ###

Edit the script and change the variables :


* __``FLASH``__ : PATH to destination working directory
* __``TEMP``__ : PATH to source working directory
* __``DEST_REPO``__ : URL to destination git repository

**Note** : All the commit will be taken from a branch named *develop*

#### Future ##

* Todo : create the parameter for the list of the source repository 

### texplode.sh

Shell xml parser and sub module maven project builder

