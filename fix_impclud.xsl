<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:param name="ROOT" select="'ecmRes:'"></xsl:param>
    <xsl:template match="import|include">
        <xsl:variable name="rootHrf" select="@href"/>
        <xsl:variable name="jar" select="substring-before(@href,':')"/>
        <xsl:if test="string-length($jar)=0">
            <xsl:element name="xsl:import">
                <xsl:attribute name="href">
                    <xsl:value-of select="concat($ROOT,@href)"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:if>
        <xsl:if test="string-length($jar)>0">
            <xsl:copy>
                <xsl:apply-templates  select="@*|node()"/>
            </xsl:copy>
        </xsl:if>
    </xsl:template>
    <xsl:template match ="@*|node()">
        <xsl:copy>
            <xsl:apply-templates  select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>