#!/bin/bash

TEE_ROOT=/d/FLASH/build/git/SIE-XMLFirst/sie-xf-xdev-flash/modules/sie-xf-ecm-resources/src/main/TEE-conf
TEE_DEST=/D/FLASH/temp/flash/modules/TEE

declare -A config
config["xf-ecmRes"]=/d/FLASH/build/git/SIE-XMLFirst/sie-xf-xdev-flash/modules/sie-xf-ecm-resources/src/main/resources
config["xfe"]=/d/FLASH/build/git/SIE-XMLFirst/sie-xf-xdev-flash/modules/sie-xf-editor/src/main/

saxon () {
	CLASSPATH=""
	for a in exec/*.jar ;do CLASSPATH="./$a;$CLASSPATH" ; done
	for a in exec/xmlresolver.dependancies/*.jar ; do  CLASSPATH="./$a;$CLASSPATH" ; done
	for a in exec/xmlresolver.dependancies/httpclient.dependencies/*.jar; do CLASSPATH="./$a;$CLASSPATH" ; done

	SAXONOPT="-versionmsg:off -expand:off -r:org.xmlresolver.Resolver"
	JAVAOPT="-Xms512m -Xmx1024m -Dlog4j.configuration=log4j.properties -classpath $CLASSPATH"

	ab="$1"
	vin=`echo ${ab#*d/}`
	vin2="d:/$vin"
	out="./" #`dirname "$1"`
	out="$out`basename "$1"`"
	#echo "$vin $vin2 $out"
	JAVA="java -classpath $CLASSPATH net.sf.saxon.Transform  -u -s:file:$vin2 -xsl:./$2 -o:$3 $4"
	echo "$(tput setaf 4 dim)[INFO]$(tput setaf 7 dim) Saxon Transform  : $(tput setaf 2 dim)$vin2 $2 $3 $4"
	tput setaf 7 sgr0
	$JAVA

}


fix_mv_srcres() {
	eval $1
	jar_root=`echo $jarhref | cut -f1 -d'/'`
    path=`echo ${jarhref#*/}`
    common=`echo $path | cut -f1 -d'/'`
    if [[ "$common" != "_common" ]] ; then
    	echo "$(tput setaf 4 dim)[INFO]$(tput setaf 7 dim) Handling : $(tput setaf 2 dim)$path"
    	tput setaf 7 sgr0
    	root=${config["$jar_root"]}
    	if [[ -e "$root/$path" ]] ; then
    		xslname=`basename $path`
    		saxon $root/$path fix_impclud.xsl ./$xslname "ROOT='ecmRes:'"
    		if [[ -e "./$xslname" ]] ; then
    			mv ./$xslname $2
    		else
    			echo "$(tput setaf 1 red)[ERROR] $(tput setaf 3 dim) Saxon have not generate : $xslname"
    		fi
    	else
    		echo "$(tput setaf 1 red)[ERROR] $(tput setaf 3 dim) { $type } { $jarhref }"
    		tput setaf 7 sgr0
    	fi
    fi
    
}
move_srcres() {
	eval $1
	jar_root=`echo $jarhref | cut -f1 -d'/'`
    path=`echo ${jarhref#*/}`
    common=`echo $path | cut -f1 -d'/'`
    if [[ "$common" != "_common" ]] ; then
    	echo "$(tput setaf 4 dim)[INFO]$(tput setaf 7 dim) copying : $(tput setaf 2 dim)$path"
    	tput setaf 7 sgr0
    	root=${config["$jar_root"]}
    	if [[ -e "$root/$path" ]] ; then
    		cp $root/$path $2
    	else
    		echo "$(tput setaf 1 red)[ERROR] $(tput setaf 3 dim) { $type } { $jarhref }"
    		tput setaf 7 sgr0
    	fi
    fi
}
read_dom () {
    local IFS=\>
    read -d \< ENTITY CONTENT
    local ret=$?
    TAG_NAME=${ENTITY%% *}
    BF=${ENTITY#* }
    search="jar-href"
    replace="jarhref"
    ATTRIBUTES="${BF/$search/$replace}"
    #echo "Attributs $ATTRIBUTES"
    #echo "BF $BF"
    return $ret
}

parse_dom () {
   	check="${ATTRIBUTES: -1}"
	ATT=$ATTRIBUTES
	if [[ $check = "/" ]] ; then
		ATT="${ATTRIBUTES:0:-1}"
	fi
    if [[ $TAG_NAME = "resource-xslt" ]] ; then
        fix_mv_srcres "$ATT" $TEE_DEST/$1/src/main/xsl/
    elif [[ $TAG_NAME = "resource-css" ]] ; then
        move_srcres "$ATT"  $TEE_DEST/$1/src/main/resources/
    elif [[ $TAG_NAME = "resource-schema" ]] ; then
        move_srcres "$ATT"  $TEE_DEST/$1/src/main/grammar/
    fi
}    


function makerepo {
	echo "======================================"
	module_name=`basename $1`
	echo "[INFO] New TEE : $1"
	mkdir -p $TEE_DEST/$module_name/src/main/xsl
	mkdir -p $TEE_DEST/$module_name/src/main/resources
	mkdir -p $TEE_DEST/$module_name/src/main/grammar
	mkdir -p $TEE_DEST/$module_name/src/test/sample
	mkdir -p $TEE_DEST/$module_name/src/test/xspec

	echo "[INFO] Copying POM"
	saxon "`pwd`/pom.xml" ./generate_pom.xsl ./"pom_$module_name.xml" "TEE_NAME=$module_name"
	mv ./"pom_$module_name.xml" $TEE_DEST/$module_name/pom.xml 

	echo "[INFO] Parsing : $1"
	cp $1 $TEE_DEST/$module_name/src/main/resources
	while read_dom; do
    	parse_dom $module_name
	done < $1
	echo "######################################"
}

figlet TEE PROJECT MAKER

echo "[INFO] Destination : $TEE_DEST"
if [[ -e "$TEE_DEST" ]] ; then
	rm -rf $TEE_DEST
fi
mkdir -p $TEE_DEST
for a in $TEE_ROOT/*.xml
do
	makerepo $a
done
