#!/bin/bash

FLASH=/d/FLASH/build/temp/flash/
TEMP=/D/FLASH/build/temp/git/

DEST_REPO = https://remileonard@bitbucket.org/elsgestion/sie-xf-xdev-flash.git

function logxml {
	cd $TEMP/$1
	echo "<?xml version='1.0' encoding='UTF-8'?><gitlog repos='`pwd`'>" >$2 ; git log --date='iso' --pretty=format:"#*#entry id='%H'#+#%n#*#author#+#%an#*#/author#+#%n#*#commit_date#+#%ct#*#/commit_date#+#%n#*#message_body#+#%B#*#/message_body#+#%n#*#/entry#+#" | sed 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g; s/\#\*\#/</g; s/#+#/>/g;' >>$2 ; echo "</gitlog>" >>$2
}
function logshell {
	cd $TEMP/$1
	git checkout $3
	git log --pretty=format:"%ct#%H#$1#$4" >$2
	echo "" >> $2
}

# 1 : repo distant
# 2 : module local
# 3 : commit source
# 4 : message
# 5 : auteur
# 6 : date

function copie_commit {
	echo "================================================================="
	echo "** Start **"
	echo "Repos : $1"
	echo "Commit : $3"
	cd $TEMP/$1
	echo "[Checkout]"
	git checkout $3
	cdate=`git log $3 -1 --pretty="%ci"`
	cauth=`git log $3 -1 --pretty="%aN <%ae>"`
	cmess=`git log $3 -1 --pretty="%B"`
	echo "[Rsync]"
	rsync -aqr --delete-before $TEMP/$1 $FLASH/modules/
	rm -rf  "$FLASH/modules/$1/.git/"
	cd $FLASH/
	git add .
	git commit -m "$cmess" --author="$cauth" --date="$cdate"
	echo "** End **"
}
figlet Initialisation

rm -rf $FLASH
mkdir -p $FLASH/

cd $FLASH
git clone $DEST_REPO

FLASH=$FLASH/sie-xf-xdev-flash/
mkdir -p $FLASH/modules

figlet Get git Repos
rm -rf $TEMP
mkdir -p $TEMP
cd $TEMP

git clone https://remileonard@bitbucket.org/elsgestion/sie-xf-sas-conversion.git
git clone https://remileonard@bitbucket.org/elsgestion/sie-xf-ecm-resources.git
git clone https://remileonard@bitbucket.org/elsgestion/sie-xf-editor.git
git clone https://remileonard@bitbucket.org/elsgestion/sie-xf-models.git

figlet Commit log
logshell sie-xf-models /D/FLASH/build/temp/xf-models.log develop xf-models
logshell sie-xf-sas-conversion /D/FLASH/build/temp/xf-sas.log  develop xf-sas
logshell sie-xf-ecm-resources /D/FLASH/build/temp/xf-ecmRes.log develop xf-ecmRes
logshell sie-xf-editor /D/FLASH/build/temp/xfe.log develop xfe

cd /D/FLASH/build/temp/
figlet Sorting
cat /D/FLASH/build/temp/xf-models.log /D/FLASH/build/temp/xf-sas.log /D/FLASH/build/temp/xf-ecmRes.log /D/FLASH/build/temp/xfe.log | sort -t'#' > /D/FLASH/build/temp/flash.log

figlet Apply

SECONDS=0

for a in `cat /D/FLASH/build/temp/flash.log`
do
	commit=`echo $a | cut -f2 -d"#"`
	repos=`echo $a | cut -f3 -d"#"`
	dest=`echo $a | cut -f4 -d"#"`
	copie_commit "$repos" "$dest" "$commit"
done




figlet pushing

cd $FLASH
git push

figlet FLASH OK
duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

