<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://maven.apache.org/POM/4.0.0"
    version="2.0">
    <xsl:param name="TEE_NAME" select="'TEE NAME'"/>
    

    <xsl:template match="/project/artifactId">
        <artifactId>
            <xsl:value-of select="$TEE_NAME"/>
        </artifactId>
    </xsl:template>
    <xsl:template match="/project/name">
        <name>
            <xsl:value-of select="$TEE_NAME"/>
        </name>
    </xsl:template>
    <xsl:template match ="@*|node()">
        <xsl:copy>
            <xsl:apply-templates  select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>